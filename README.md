# README #

In A Viewport

### SUMMARY ###

* This repo will be used for inaviewport.com Wordpress theme devlopement

### USERS AND BRANCHES ###

* The master branch will be owned and controlled by Van Gould

### Contribution guidelines ###

* Please make sure that all commits have specific comments

### CONTACT INFO ###

* Van Gould
* 619-436-7978
* a.vangould@gmail.com
* Slack @vangould